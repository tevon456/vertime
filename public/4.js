(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/src/components/pages/Account.js":
/*!******************************************************!*\
  !*** ./resources/js/src/components/pages/Account.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ui/inputs/Form */ "./resources/js/src/components/ui/inputs/Form.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../App */ "./resources/js/src/App.js");
/* harmony import */ var _ui_content_Avatar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ui/content/Avatar */ "./resources/js/src/components/ui/content/Avatar.js");
/* harmony import */ var _ui_surfaces_Badge__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../ui/surfaces/Badge */ "./resources/js/src/components/ui/surfaces/Badge.js");
/* harmony import */ var _ui_content_Container__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ui/content/Container */ "./resources/js/src/components/ui/content/Container.js");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-chartjs-2 */ "./node_modules/react-chartjs-2/es/index.js");
/* harmony import */ var _ui_content_Error__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../ui/content/Error */ "./resources/js/src/components/ui/content/Error.js");
/* harmony import */ var _ui_content_Footer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../ui/content/Footer */ "./resources/js/src/components/ui/content/Footer.js");
/* harmony import */ var _elements_Head__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./../elements/Head */ "./resources/js/src/components/elements/Head.js");
/* harmony import */ var _ui_content_Icon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../ui/content/Icon */ "./resources/js/src/components/ui/content/Icon.js");
/* harmony import */ var _ui_content_Line__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../ui/content/Line */ "./resources/js/src/components/ui/content/Line.js");
/* harmony import */ var _ui_content_Note__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../ui/content/Note */ "./resources/js/src/components/ui/content/Note.js");
/* harmony import */ var _elements_Page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../elements/Page */ "./resources/js/src/components/elements/Page.js");
/* harmony import */ var _ui_content_WhiteSpace__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../ui/content/WhiteSpace */ "./resources/js/src/components/ui/content/WhiteSpace.js");
/* harmony import */ var _helper_themeHelper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../helper/themeHelper */ "./resources/js/src/helper/themeHelper.js");
/* harmony import */ var _ui_inputs_Toggle__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../ui/inputs/Toggle */ "./resources/js/src/components/ui/inputs/Toggle.js");
/* harmony import */ var _helper_authHelper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../helper/authHelper */ "./resources/js/src/helper/authHelper.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





















var Account =
/*#__PURE__*/
function (_Component) {
  _inherits(Account, _Component);

  function Account(props) {
    var _this;

    _classCallCheck(this, Account);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Account).call(this, props));
    _this.state = {
      isEditingName: false,
      isEditingEmail: false,
      isEditingAvatar: false,
      isEditingCell: false
    };
    _this.setState = _this.setState.bind(_assertThisInitialized(_this));
    _this.ToggleAvatar = _this.ToggleAvatar.bind(_assertThisInitialized(_this));
    _this.ToggleName = _this.ToggleName.bind(_assertThisInitialized(_this));
    _this.ToggleEmail = _this.ToggleEmail.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Account, [{
    key: "ToggleAvatar",
    value: function ToggleAvatar(state) {
      if (state == true) {
        this.setState({
          isEditingAvatar: false
        });
      } else {
        this.setState({
          isEditingAvatar: true
        });
      }
    }
  }, {
    key: "ToggleCell",
    value: function ToggleCell(state) {
      if (state == true) {
        this.setState({
          isEditingCell: false
        });
      } else {
        this.setState({
          isEditingCell: true
        });
      }
    }
  }, {
    key: "ToggleName",
    value: function ToggleName(state) {
      if (state == true) {
        this.setState({
          isEditingName: false
        });
      } else {
        this.setState({
          isEditingName: true
        });
      }
    }
  }, {
    key: "ToggleEmail",
    value: function ToggleEmail(state) {
      if (state == true) {
        this.setState({
          isEditingEmail: false
        });
      } else {
        this.setState({
          isEditingEmail: true
        });
      }
    }
  }, {
    key: "logUserOut",
    value: function logUserOut() {
      Object(_helper_authHelper__WEBPACK_IMPORTED_MODULE_17__["deauthenticateUser"])();
      setTimeout(function () {
        location.reload();
      }, 2000);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Error__WEBPACK_IMPORTED_MODULE_7__["default"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_App__WEBPACK_IMPORTED_MODULE_2__["AppContext"].Consumer, null, function (context) {
        return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_elements_Page__WEBPACK_IMPORTED_MODULE_13__["default"], {
          padding: true
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_elements_Head__WEBPACK_IMPORTED_MODULE_9__["default"], {
          title: "Account"
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Account"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "btns"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
          onClick: _this2.logUserOut,
          className: "btn btn--secondary btn--sm text--md"
        }, "Logout")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Container__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(EditSectionAction, {
          state: _this2.state.isEditingAvatar,
          action: function action() {
            return _this2.ToggleAvatar(_this2.state.isEditingAvatar);
          }
        }), _this2.state.isEditingAvatar ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "grid-center animated fadeIn"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__["ProfileImageUpload"], null)) : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "grid-center animated fadeIn"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "margin-bottom--md"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Avatar__WEBPACK_IMPORTED_MODULE_3__["Avatar"], {
          style: {
            display: "grid"
          },
          first_name: context.state.user.first_name,
          last_name: context.state.user.last_name,
          size: "huge",
          src: context.state.user.avatar
        }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Line__WEBPACK_IMPORTED_MODULE_11__["Line"], {
          className: "margin-top--md"
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(EditSectionAction, {
          state: _this2.state.isEditingName,
          action: function action() {
            return _this2.ToggleName(_this2.state.isEditingName);
          }
        }), _this2.state.isEditingName ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__["OverideForm"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__["UpdateNameForm"], {
          data: context.state.user
        })) : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
          className: "margin-top--md margin-bottom--none"
        }, "Name"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, context.state.user.first_name, " ", context.state.user.last_name)), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Line__WEBPACK_IMPORTED_MODULE_11__["Line"], {
          className: "margin-top--md"
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(EditSectionAction, {
          state: _this2.state.isEditingCell,
          action: function action() {
            return _this2.ToggleCell(_this2.state.isEditingCell);
          }
        }), _this2.state.isEditingCell ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__["OverideForm"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__["UpdateCellForm"], {
          data: context.state.user
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Form__WEBPACK_IMPORTED_MODULE_0__["VerificationCodeForm"], null)), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Note__WEBPACK_IMPORTED_MODULE_12__["default"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", null, "Note"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), "When adding or updating your number a verification code will be sent to ensure that you own the number.")) : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
          className: "margin-top--md margin-bottom--none"
        }, "Cell phone number", " ", context.state.user.cell ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
          "aria-label": "number verified",
          "data-balloon-pos": "up"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_surfaces_Badge__WEBPACK_IMPORTED_MODULE_4__["default"], {
          background: "var(--green)"
        }, "verified")) : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
          "aria-label": "number not added",
          "data-balloon-pos": "up"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_surfaces_Badge__WEBPACK_IMPORTED_MODULE_4__["default"], {
          background: "red"
        }, "no number"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", null, context.state.user.cell))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", null, "Settings"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Container__WEBPACK_IMPORTED_MODULE_5__["default"], null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          style: {
            display: "flex",
            justifyContent: "space-between"
          }
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("b", {
          className: "margin-top--sm margin-bottom--md"
        }, "Dark mode", " ", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
          "aria-label": "reduces eye strain when enabled at night",
          "data-balloon-pos": "up"
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_surfaces_Badge__WEBPACK_IMPORTED_MODULE_4__["default"], null, "?"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_inputs_Toggle__WEBPACK_IMPORTED_MODULE_16__["default"], {
          on: _helper_themeHelper__WEBPACK_IMPORTED_MODULE_15__["default"].get(),
          switchOn: function switchOn() {
            return _helper_themeHelper__WEBPACK_IMPORTED_MODULE_15__["default"].set();
          },
          switchOff: function switchOff() {
            return _helper_themeHelper__WEBPACK_IMPORTED_MODULE_15__["default"].set();
          }
        })))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Footer__WEBPACK_IMPORTED_MODULE_8__["default"], null));
      }));
    }
  }]);

  return Account;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Account);

function EditSectionAction(props) {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: {
      display: "flex",
      justifyContent: "flex-end"
    }
  }, props.state ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: props.action,
    className: "btn btn--bordered btn--sm text--md"
  }, "Cancel") : react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    onClick: props.action,
    className: "btn btn--bordered btn--sm text--md"
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_Icon__WEBPACK_IMPORTED_MODULE_10__["IconEdit"], null), " ", react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_content_WhiteSpace__WEBPACK_IMPORTED_MODULE_14__["default"], {
    amount: 1
  }), " Edit"));
}

/***/ }),

/***/ "./resources/js/src/components/ui/content/Footer.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/ui/content/Footer.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navigation_CustomLink__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../navigation/CustomLink */ "./resources/js/src/components/ui/navigation/CustomLink.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _constants_Routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../constants/Routes */ "./resources/js/src/constants/Routes.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    background: var(--black);\n\n    .footer,\n    .fh,\n    .fp {\n        color: var(--white) !important;\n    }\n\n    .footer-section-head {\n        border-bottom: 2px solid var(--white);\n        padding-bottom: var(--space-xs);\n        font-weight: bold;\n    }\n    .footer-list {\n        list-style-type: none;\n        margin: 1.5em 3.2em;\n        margin-left: -10px;\n    }\n\n    .footer-list li {\n        padding: 6px 4px;\n    }\n\n    .footer-list li a {\n        color: var(--white);\n        text-decoration: none;\n        padding: 2px 2px;\n    }\n\n    .footer-list li a:hover {\n        color: var(--accent-color);\n    }\n\n    .footer-section {\n        display: flex;\n        flex-wrap: wrap;\n        -webkit-align-items: baseline;\n        -ms-flex-align: baseline;\n        align-items: baseline;\n        padding-bottom: var(--space-xxl);\n    }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }






var StyledFooter = styled_components__WEBPACK_IMPORTED_MODULE_4__["default"].div(_templateObject());
var date = new Date();

var Footer = function Footer() {
  return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(StyledFooter, {
    className: "grid-center"
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "footer-section "
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("ul", {
    className: "footer-list"
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h5", {
    className: "footer-section-head fh"
  }, "Company")), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_navigation_CustomLink__WEBPACK_IMPORTED_MODULE_0__["ExternalLink"], {
    to: "mailto:enproperty.co@gmail.com",
    color: "var(--white)"
  }, "Contact us")), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    tabIndex: "0",
    to: _constants_Routes__WEBPACK_IMPORTED_MODULE_3__["TERMS"]
  }, "Terms of service"))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("ul", {
    className: "footer-list"
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("h5", {
    className: "footer-section-head fh"
  }, "Product")), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_navigation_CustomLink__WEBPACK_IMPORTED_MODULE_0__["ExternalLink"], {
    to: "https://enproperty-co.typeform.com/to/Eyhj8j",
    color: "var(--white)"
  }, "Feedback")), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_navigation_CustomLink__WEBPACK_IMPORTED_MODULE_0__["ExternalLink"], {
    to: "https://enproperty.crisp.watch/en/",
    color: "var(--white)"
  }, "Status")))), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "grid-center footer-legal"
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("p", {
    className: "fp"
  }, "Made with \u2764 in Kingston, Jamaica")), react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
    className: "grid-center footer-legal margin-bottom--lg"
  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("p", {
    className: "fp"
  }, "\xA9 Copyright 2018 - ", date.getFullYear(), " Enproperty")));
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./resources/js/src/components/ui/content/Line.js":
/*!********************************************************!*\
  !*** ./resources/js/src/components/ui/content/Line.js ***!
  \********************************************************/
/*! exports provided: Line */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Line", function() { return Line; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function Line() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "margin-bottom--sm",
    style: {
      borderBottom: "1px solid var(--light-grey)"
    }
  });
}

/***/ }),

/***/ "./resources/js/src/components/ui/content/WhiteSpace.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/components/ui/content/WhiteSpace.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var wspace = "\xA0";

var Space = function Space(_ref) {
  var amount = _ref.amount;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, wspace.repeat(amount == undefined ? 1 : amount));
};

/* harmony default export */ __webpack_exports__["default"] = (Space);

/***/ }),

/***/ "./resources/js/src/components/ui/inputs/Toggle.js":
/*!*********************************************************!*\
  !*** ./resources/js/src/components/ui/inputs/Toggle.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    position: relative;\n    display: block;\n    line-height: 2;\n    width: 60px;\n    height: 30px;\n    margin: var(--space-xxs);\n    margin-left: 0px;\n\n    input {\n        opacity: 0;\n        width: 0;\n    }\n\n    .slider {\n        position: absolute;\n        cursor: pointer;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        -webkit-transition: 0.4s;\n        transition: 0.4s;\n    }\n\n    .slider:before {\n        position: absolute;\n        box-shadow: 0px 2px 4px rgba(60, 60, 60, 0.73),\n            0px 2px 4px rgba(71, 63, 79, 0.08);\n        content: \"\";\n        height: 22px;\n        width: 22px;\n        left: 6px;\n        bottom: 4px;\n        background-color: var(--white);\n        background-image: linear-gradient(#fff, #fff);\n        -webkit-transition: 0.5s;\n        transition: 0.5s;\n        transition-timing-function: cubic-bezier(0.27, 0.88, 0.27, 0.88);\n    }\n\n    & input:checked + .slider {\n        background-color: var(--accent-color-light);\n        ", "\n    }\n\n    & input:not(:checked) + .slider {\n        background-color: #909090;\n    }\n\n    & input:checked + .slider:before {\n        -webkit-transform: translateX(26px);\n        -ms-transform: translateX(26px);\n        transform: translateX(26px);\n    }\n\n    .slider.round {\n        border-radius: 50px;\n    }\n\n    .slider.round:before {\n        border-radius: 50px;\n    }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var Toggle =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Toggle, _React$Component);

  function Toggle(props) {
    var _this;

    _classCallCheck(this, Toggle);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Toggle).call(this, props));
    _this.state = {
      checked: _this.props.on ? true : false
    };
    _this.mainCall = _this.mainCall.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Toggle, [{
    key: "toggle",
    value: function toggle() {
      var _this2 = this;

      this.setState(function () {
        return {
          checked: !_this2.state.checked
        };
      });
    }
  }, {
    key: "noFunction",
    value: function noFunction() {
      console.log("No function passed to this toggle for execution");
    }
  }, {
    key: "mainCall",
    value: function mainCall() {
      if (this.props.switchOn && this.state.checked == false) {
        this.props.switchOn();
      } else if (this.props.switchOff && this.state.checked == true) {
        this.props.switchOff();
      } else {
        this.noFunction;
      }
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyledToggle, {
        color: this.props.color
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        type: "checkbox",
        onClick: this.toggle.bind(this),
        onChange: this.mainCall,
        checked: this.state.checked
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "slider round"
      }));
    }
  }]);

  return Toggle;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

var StyledToggle = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].label(_templateObject(), function (_ref) {
  var color = _ref.color;
  return color && "\n    background: ".concat(color || "var(--accent-color-light)", ";\n  ");
});
/* harmony default export */ __webpack_exports__["default"] = (Toggle);

/***/ }),

/***/ "./resources/js/src/components/ui/surfaces/Badge.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/components/ui/surfaces/Badge.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
function _templateObject() {
  var data = _taggedTemplateLiteral(["\n        color: var(--primary-color-2);\n        background: ", ";\n        border-radius: var(--radius);\n        padding: 2px 8px;\n        margin: 2px;\n        cursor: default;\n        display: inline-block;\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }



/**
 * Text badge that renders children
 */

function Badge(props) {
  var BadgeStyle = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].span(_templateObject(), props.background || "var(--secondary-color)");
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(BadgeStyle, null, props.children);
}

/* harmony default export */ __webpack_exports__["default"] = (Badge);

/***/ }),

/***/ "./resources/js/src/helper/themeHelper.js":
/*!************************************************!*\
  !*** ./resources/js/src/helper/themeHelper.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// @ts-check
var Theme =
/*#__PURE__*/
function () {
  function Theme() {
    _classCallCheck(this, Theme);
  }

  _createClass(Theme, null, [{
    key: "set",

    /**
     * Toggles between dark and light theme
     */
    value: function set() {
      localStorage.getItem("dark") === "true" ? localStorage.setItem("dark", "false") : localStorage.setItem("dark", "true");
      Theme.apply();
    }
    /**
     * Returns true if theme is dark
     * @returns {boolean}
     */

  }, {
    key: "get",
    value: function get() {
      return localStorage.getItem("dark") !== "false";
    }
    /**
     * Responsible for applying theme based on theme value
     */

  }, {
    key: "apply",
    value: function apply() {
      var root = document.documentElement.style;

      if (localStorage.getItem("dark") === "true") {
        root.setProperty("--primary-color", "#383838");
        root.setProperty("--primary-color-2", "#272727");
        root.setProperty("--secondary-color", "#f2f2f2");
        root.setProperty("--grey", "#E0E0E0");
        root.setProperty("--alpha", "0");
        root.setProperty("--btn-secondary-bg", "#616161");
      } else {
        root.setProperty("--primary-color", "#f2f2f2");
        root.setProperty("--primary-color-2", "#fff");
        root.setProperty("--grey", "#999999");
        root.setProperty("--secondary-color", "#131313");
        root.setProperty("--alpha", "1");
        root.setProperty("--btn-secondary-bg", "var(--black)");
      }
    }
  }]);

  return Theme;
}();

/* harmony default export */ __webpack_exports__["default"] = (Theme);

/***/ })

}]);