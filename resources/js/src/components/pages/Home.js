import { FlexItem, FlexRow } from "../elements/FlexView";
import { LISTING, LOGIN, SIGN_UP } from "../../constants/Routes";
import Page, { Background } from "../elements/Page";

import { BaseCard } from "../ui/surfaces/Card";
import Footer from "../ui/content/Footer";
import Head from "../elements/Head";
import { IconChevronRight } from "../ui/content/Icon";
import { Line } from "../ui/content/Line";
import { Link } from "react-router-dom";
import React from "react";
import Space from "../ui/content/WhiteSpace";
import { device } from "./../../constants/devices";
import styled from "styled-components";

function Home() {
    return (
        <React.Fragment>
            <Background
                src={`/images/illustrations/Waiau.svg`}
                color="var(--light-grey)"
                height="90vh"
                repeat="no-repeat"
                cover="cover"
            >
                <Page padding={true}>
                    <Head title="Enproperty - Free Jamaican rental listing" />
                    <div
                        className="margin-top--none"
                        style={{ height: "100vh" }}
                    >
                        <div
                            style={{
                                background: "#2c2f33",
                                padding: "2%",
                                maxWidth: "450px",
                                maxHeight: "500px"
                            }}
                        >
                            <div>
                                <h1
                                    style={{
                                        color: "var(--white)"
                                    }}
                                >
                                    Simple, easy free rental listing
                                </h1>
                            </div>
                            <p
                                style={{
                                    color: "var(--white)",
                                    maxWidth: "400px"
                                }}
                                className="text--bold text--md margin-bottom--xl"
                            >
                                Built for small landlords and just about anyone
                                to easily list their rental units.
                            </p>
                            <div className="btns">
                                <Link
                                    tabIndex="0"
                                    to={SIGN_UP}
                                    style={{
                                        background: "#fff",
                                        color: "var(--black)"
                                    }}
                                    className="btn btn--primary btn--md "
                                >
                                    Sign up
                                </Link>
                                <Link
                                    tabIndex="0"
                                    to={LISTING}
                                    className="btn btn--secondary btn--md "
                                >
                                    View listing
                                </Link>
                            </div>

                            <p
                                style={{
                                    color: "var(--white)"
                                }}
                                className="text--rg"
                            >
                                Already have an account?{" "}
                                <Link
                                    style={{ color: "var(--white)" }}
                                    to={LOGIN}
                                >
                                    Login
                                </Link>
                            </p>
                        </div>
                    </div>
                </Page>
            </Background>

            <Section background="#2c2f33">
                <div className="margin-top--lg" />
                <SectionFlex>
                    <div>
                        <h2 className="text--white">Built for you</h2>
                        <h5
                            style={{ maxWidth: "340px", color: "#a1aebb" }}
                            className="margin-bottom--xl"
                        >
                            If you've been using excel sheets, Whatsapp groups
                            and wall posters to list your rental units and their
                            availabilty, this tool was built for you.
                            <br />
                            <br />
                            Now you can easily list your unit online and manage
                            unit availability.
                        </h5>

                        <div className="btns">
                            <Link
                                to={SIGN_UP}
                                tabIndex="0"
                                style={{
                                    background: "#fff",
                                    color: "var(--black)"
                                }}
                                className="btn btn--primary btn--full-width "
                            >
                                Get Started
                            </Link>
                        </div>
                    </div>
                    <Space amount={8} />
                    <div style={{ borderLeft: "1px solid #a1aebb" }} />
                    <Space amount={8} />
                    <span className="grid-center">
                        <VideoEmbed />
                    </span>
                </SectionFlex>
                <div className="margin-bottom--lg" />
            </Section>

            <Line />

            <Section className="margin-bottom--xl">
                <h2 className="text--center margin-bottom--sm">Features</h2>
                <SectionFlex>
                    <BaseCard
                        maxWidth="300px"
                        style={{ height: "300px", margin: "18px" }}
                    >
                        <div className="grid-center">
                            <img
                                width="140px"
                                height="140px"
                                src="/images/illustrations/landing/add_property.png"
                            />
                            <br />
                        </div>
                        <InnerCard head="Add properties" headColor="#5626BB">
                            Easily add your property that has units for rent.
                        </InnerCard>
                    </BaseCard>
                    <BaseCard
                        maxWidth="300px"
                        style={{ height: "300px", margin: "18px" }}
                    >
                        <div className="grid-center">
                            <img
                                width="140px"
                                height="140px"
                                src="/images/illustrations/landing/property_unit.png"
                            />
                            <br />
                        </div>
                        <InnerCard
                            head="Add property units"
                            headColor="#5626BB"
                        >
                            These are individual sections of a property being
                            rented.
                        </InnerCard>
                    </BaseCard>
                    <BaseCard
                        maxWidth="300px"
                        style={{ height: "300px", margin: "18px" }}
                    >
                        <div className="grid-center">
                            <img
                                width="140px"
                                height="140px"
                                src="/images/illustrations/landing/list_delist.png"
                            />
                            <br />
                        </div>
                        <InnerCard head="List and delist" headColor="#5626BB">
                            When a unit becomes occupied by a tenant you can
                            delist it then list it again when available.
                        </InnerCard>
                    </BaseCard>
                    <BaseCard
                        maxWidth="300px"
                        style={{ height: "300px", margin: "18px" }}
                    >
                        <div className="grid-center">
                            <img
                                width="140px"
                                height="140px"
                                src="/images/illustrations/landing/manage_images.png"
                            />
                            <br />
                        </div>
                        <InnerCard head="Manage images" headColor="#5626BB">
                            Easily update unit images or remove old ones.
                        </InnerCard>
                    </BaseCard>
                    <BaseCard
                        maxWidth="300px"
                        style={{ height: "300px", margin: "18px" }}
                    >
                        <div className="grid-center">
                            <img
                                width="140px"
                                height="140px"
                                src="/images/illustrations/landing/edit_delete.png"
                            />
                            <br />
                        </div>
                        <InnerCard head="Edit & Delete" headColor="#5626BB">
                            Made an error? Don't worry you can edit and delete
                            at anytime.
                        </InnerCard>
                    </BaseCard>
                    <BaseCard
                        maxWidth="300px"
                        style={{ height: "300px", margin: "18px" }}
                    >
                        <div className="grid-center">
                            <img
                                width="140px"
                                height="140px"
                                src="/images/illustrations/landing/contact.png"
                            />
                            <br />
                        </div>
                        <InnerCard head="Public listing" headColor="#5626BB">
                            Be discovered on our listing page where future
                            tenants can contact you.
                        </InnerCard>
                    </BaseCard>
                </SectionFlex>
                <div className="margin-bottom--xxl" />
            </Section>

            <Line />

            <Section className="margin-bottom--xl">
                <h2 className="text--center margin-bottom--sm">Plus more</h2>

                <FlexRow>
                    <FlexItem>
                        {" "}
                        <BaseCard maxWidth="300px" style={{ height: "190px" }}>
                            <InnerCard head="Bookmark for later">
                                You can bookmark units you are interested in for
                                later viewing.
                            </InnerCard>
                        </BaseCard>
                    </FlexItem>

                    <FlexItem>
                        <BaseCard maxWidth="300px" style={{ height: "190px" }}>
                            <InnerCard head="Live chat support">
                                If you ever need any help our live chat bubble
                                is always on the lower right corner.
                            </InnerCard>
                        </BaseCard>
                    </FlexItem>

                    <FlexItem>
                        <BaseCard maxWidth="300px" style={{ height: "190px" }}>
                            <InnerCard head="Night mode">
                                Reduce eye strain when using our website at
                                night.
                            </InnerCard>
                        </BaseCard>
                    </FlexItem>
                    <FlexItem>
                        <BaseCard maxWidth="300px" style={{ height: "190px" }}>
                            <InnerCard head="More to come">
                                We are still working on new features.
                            </InnerCard>
                        </BaseCard>
                    </FlexItem>
                </FlexRow>
                <div className="margin-bottom--xl" />
            </Section>

            <Line />

            <Section background="var(--accent-color-light)" padding="2%">
                <span className="grid-center">
                    <h2 className="text--center margin-bottom--xxs text--white">
                        Still not sure? Take it for a test
                    </h2>
                    <p className="text--center text--white">it's free</p>
                    <div className="btns">
                        <button className="btn btn--secondary btn--full-width ">
                            I'm ready <IconChevronRight strokeWidth={4} />
                        </button>
                    </div>
                </span>
            </Section>
            <Footer />
        </React.Fragment>
    );
}
export default Home;

function Section(props) {
    return (
        <section
            style={{
                background: props.background || "var(--white)",
                padding: props.padding || "2% 4%"
            }}
        >
            {props.children}
        </section>
    );
}

function VideoEmbed() {
    const Frame = styled.iframe`
        width: 640px;
        height: 497px;
        border-radius: 4px;

        @media ${device.tabletS} {
            width: 400px;
            height: 350px;
        }

        @media ${device.mobileM} {
            width: 300px;
            height: 300px;
        }
    `;
    return (
        <Frame
            src="https://player.vimeo.com/video/366353701"
            frameborder="0"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowfullscreen
        />
    );
}

function SectionFlex(props) {
    return (
        <section
            style={{
                display: "flex",
                flexWrap: "wrap",
                flexDirection: "row",
                justifyContent: props.justify || "center"
            }}
        >
            {props.children}
        </section>
    );
}

function InnerCard(props) {
    return (
        <React.Fragment>
            <div className=" margin-bottom--md text--center ">
                <span
                    style={{
                        color: props.headColor || "var(--accent-color-light)"
                    }}
                    className="text--md text--bold margin-bottom--md"
                >
                    {props.head}
                </span>
            </div>
            <div className=" margin-bottom--md text--left">
                <span className="text--rg">{props.children}</span>
            </div>
        </React.Fragment>
    );
}
