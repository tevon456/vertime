import { FlexItem, FlexView } from "../elements/FlexView";
import React, { useEffect, useState } from "react";

import { Avatar } from "../ui/content/Avatar";
import { BaseCard } from "../ui/surfaces/Card";
import ErrorBoundary from "./../ui/content/Error";
import { ExternalLink } from "./../ui/navigation/CustomLink";
import Footer from "../ui/content/Footer";
import Head from "../elements/Head";
import { LISTING } from "../../constants/Routes";
import { Link } from "react-router-dom";
import Page from "../elements/Page";
import Pagination from "react-js-pagination";
import { convertToSlug } from "../../helper/lib";
import { getListing } from "../../helper/apiHelper";
import styled from "styled-components";

function Listing(props) {
    //create initial state of listing
    const [listing, setListing] = useState([]);
    const [meta, setMeta] = useState([]);

    //Get data from network/api
    function fromNetwork(number) {
        getListing(number)
            .then(res => {
                setListing(res.data.data);
                setMeta(res.data.meta);
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }

    function handlePageChange(pageNumber) {
        props.history.push(`/listing/page/${pageNumber}`);
        fromNetwork(pageNumber);
        // location.assign(`/listing/page/${pageNumber}`);
    }

    useEffect(() => {
        fromNetwork(props.match.params.number);
    }, []);

    return (
        <ErrorBoundary>
            <Page padding={false}>
                <div
                    style={{
                        background: "var(--secondary-color)",
                        bottom: "251px",
                        right: "-39px",
                        transform: "rotateZ(270deg)",
                        padding: "6px",
                        height: "22px",
                        position: "fixed",
                        display: "block",
                        zIndex: "1"
                    }}
                >
                    <ExternalLink
                        color="var(--primary-color-2)"
                        to="https://enproperty-co.typeform.com/to/Eyhj8j"
                    >
                        Feedback
                    </ExternalLink>
                </div>

                <Head title="Listings" description="Find your next rental" />
                <h1 style={{ textAlign: "center" }}>Listing</h1>
                <Paginate meta={meta} handler={handlePageChange} />
                <div className="margin-bottom--xxl" />
                {listing.length > 0 ? (
                    <section className="animated fadeIn">
                        <FlexView>
                            {listing.map(d => (
                                <List key={d.id} data={d} />
                            ))}
                        </FlexView>
                    </section>
                ) : (
                    <section className="animated fadeIn delay-3s">
                        <h4 className="text--center ">No results</h4>
                        <p className="text--center">
                            This page{" "}
                            <b>
                                {`${location.origin}/page/`}
                                <span className="text--accent-light">
                                    {location.pathname.split("/")[3]}
                                </span>
                            </b>{" "}
                            does not exist as yet, <br />
                            try clicking on a page number available above or
                            below.
                        </p>
                    </section>
                )}
                <div className="margin-top--xxl" />
                {/* <Line /> */}
                <Paginate meta={meta} handler={handlePageChange} />
                <div className="margin-bottom--md" />
            </Page>
            <Footer />
        </ErrorBoundary>
    );
}

function List(props) {
    let d = props.data;
    return (
        <FlexItem>
            <BaseCard width={300} src={d.images[0] ? d.images[0].url : null}>
                <div
                    style={{
                        transform: "translateY(-65px)",
                        position: "absolute",
                        padding: "1px 5px 5px 1px",
                        display: "inline-block",
                        borderRadius: "50px",
                        background: "var(--primary-color-2)"
                    }}
                >
                    <Avatar
                        first_name={d.parent_property.owner_first_name}
                        last_name={d.parent_property.owner_last_name}
                        size="small"
                        src={d.parent_property.owner_avatar}
                    />
                </div>

                <b className="truncate-wrapper">
                    <p
                        style={{
                            marginBottom: "-0.5em"
                        }}
                        className="truncate margin-top--none"
                    >
                        {d.unit_title}
                    </p>
                </b>
                <p>
                    {d.parent_property.street}, {d.parent_property.city}
                </p>
                <div className="margin-bottom--sm margin-top--none">
                    <b>
                        <span>${Math.round(d.price_fee * 100) / 100}</span>

                        <span className="text--grey">{" | "}per month</span>
                    </b>
                </div>
                <div className="btns">
                    <Link
                        tabIndex={0}
                        to={LISTING + `/${convertToSlug(d.unit_title)}/${d.id}`}
                        className="btn btn--secondary btn--full-width "
                    >
                        view
                    </Link>
                </div>
            </BaseCard>
        </FlexItem>
    );
}

function Paginate(props) {
    const StyledPagination = styled.div`
        .pagination {
            min-width:300px;
            display: flex;
            justify-content: space-evenly;
            padding: 8px;
            list-style: none;
        }

        li > a {
            color: var(--secondary-color);
            text-decoration: none;
            font-weight: bold;
            padding:8px
            border-radius:4px;
            border: 2px solid transparent;
        }

        li:hover > a {
            border: 2px solid var(--grey);
        }

        li.active > a {
            color: var(--primary-color-2);
            border:2px solid var(--secondary-color);
            background:var(--secondary-color);
        }

        li.disabled > a {
            opacity:0.3;
            cursor:not-allowed
        }
    `;
    return (
        <StyledPagination className="grid-center">
            <Pagination
                Pagination
                prevPageText="prev"
                nextPageText="next"
                firstPageText="first"
                lastPageText="last"
                activePage={props.meta.current_page}
                itemsCountPerPage={props.meta.per_page}
                totalItemsCount={props.meta.total}
                pageRangeDisplayed={5}
                onChange={props.handler}
            />
        </StyledPagination>
    );
}

export default Listing;
