import { Avatar, AvatarLabel } from "../ui/content/Avatar";
import { DropDown, DropDownMenu, openDropDown } from "../ui/surfaces/Dropdown";
import { FlexItem, FlexRow } from "../elements/FlexView";
import { IconBookmark, IconMessage, IconPhone } from "../ui/content/Icon";
import { getUnit, likeUnit } from "../../helper/apiHelper";

import Container from "../ui/content/Container";
import { ExternalLink } from "../ui/navigation/CustomLink";
import Head from "./../elements/Head";
import { LOGIN } from "../../constants/Routes";
import { Line } from "../ui/content/Line";
import Note from "../ui/content/Note";
import Page from "../elements/Page";
import React from "react";
import { SIGN_UP } from "./../../constants/Routes";
import Space from "../ui/content/WhiteSpace";
import { isUserAuthenticated } from "../../helper/authHelper";

class ListingPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listing: [],
            isLoading: true,
            error: undefined,
            isLiked: false
        };
        this.setState = this.setState.bind(this);
        this.like = this.like.bind(this);
    }

    componentDidMount() {
        getUnit(this.props.match.params.id)
            .then(res => {
                this.setState({
                    isLoading: this.state.listing === undefined ? true : false,
                    listing: res.data.data,
                    isLiked: res.data.data.is_liked
                });
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response.status);
                    this.setState({
                        error: error.response.status
                    });
                }
            });
    }

    like() {
        likeUnit(this.state.listing.id).then(res => {
            this.setState({
                isLiked: res.data.liked
            });
        });
    }

    render() {
        let d = this.state.listing;
        let height = "80px";
        return (
            <Page
                status={this.state.error}
                padding={true}
                loading={this.state.isLoading}
            >
                <Head
                    title={d.unit_title}
                    description={d.description}
                    image={d.images ? d.images[0] : null}
                />
                {this.state.isLoading ? (
                    <div />
                ) : (
                    <section>
                        <h2 className="margin-bottom--md margin-top--sm">
                            {d.unit_title}
                        </h2>
                        {isUserAuthenticated() ? (
                            <div className="btns">
                                <DropDown>
                                    <DropDownMenu
                                        x="0px"
                                        y="27px"
                                        width="170px"
                                        tabIndex="1"
                                    >
                                        <li
                                            style={{ cursor: "initial" }}
                                            tabIndex="0"
                                            className="text--rg"
                                        >
                                            <Space amount={2} />
                                            #: {d.parent_property.owner_cell}
                                        </li>
                                        <Line />
                                        <li tabIndex="0" className="text--rg">
                                            <a
                                                href={`tel:${d.parent_property.owner_cell}`}
                                            >
                                                <IconPhone
                                                    yAxis={-1}
                                                    width="24"
                                                    heigh="24"
                                                />
                                                <Space amount={2} />
                                                Call
                                            </a>
                                        </li>

                                        <li tabIndex="0" className="text--rg">
                                            <a
                                                target="_blank"
                                                rel="noopener noreferrer"
                                                href={`https://wa.me/${d.parent_property.owner_cell}?text=Hello ${d.parent_property.owner_first_name}, I'm interested in your unit "${d.unit_title}" that is listed on https://enproperty.co`}
                                            >
                                                <IconMessage
                                                    yAxis={-1}
                                                    width="24"
                                                    heigh="24"
                                                />
                                                <Space amount={2} />
                                                Whatsapp
                                            </a>
                                        </li>
                                        <Line />
                                        <span className="text--sm margin-top--xs">
                                            whatsapp will not work if this user
                                            has no account.
                                        </span>
                                        <div className="margin-bottom--xs" />
                                    </DropDownMenu>
                                    <div
                                        title={`get in touch with ${d.parent_property.owner_first_name}`}
                                        style={{ cursor: "pointer" }}
                                        id="contact"
                                        onKeyDown={() =>
                                            openDropDown("contact")
                                        }
                                        tabIndex="0"
                                        className="btn btn--primary btn--sm"
                                    >
                                        Contact
                                    </div>
                                </DropDown>

                                <div
                                    title={
                                        this.state.isLiked
                                            ? "saved"
                                            : "save for later"
                                    }
                                    onClick={this.like}
                                >
                                    <button
                                        style={{ cursor: "pointer" }}
                                        tabIndex="0"
                                        className={` btn ${
                                            this.state.isLiked
                                                ? "btn--primary"
                                                : "btn--secondary"
                                        } btn--sm `}
                                    >
                                        <IconBookmark
                                            strokeWidth={3}
                                            fill={
                                                this.state.isLiked
                                                    ? "var(--white)"
                                                    : "none"
                                            }
                                        />
                                    </button>
                                </div>
                            </div>
                        ) : (
                            <div className="btns">
                                <DropDown>
                                    <DropDownMenu
                                        x="0px"
                                        y="27px"
                                        width="170px"
                                        tabIndex="1"
                                    >
                                        <li tabIndex="0" className="text--rg">
                                            <a href={location.origin + LOGIN}>
                                                Login
                                            </a>
                                        </li>
                                        <li tabIndex="0" className="text--rg">
                                            <a href={location.origin + SIGN_UP}>
                                                Signup
                                            </a>
                                        </li>

                                        <Line />
                                        <span className="text--sm margin-top--xs">
                                            Login or Signup to view{" "}
                                            <b>
                                                {
                                                    d.parent_property
                                                        .owner_first_name
                                                }
                                                's{" "}
                                            </b>
                                            contact details.
                                        </span>
                                        <div className="margin-bottom--xs" />
                                    </DropDownMenu>
                                    <div
                                        style={{ cursor: "pointer" }}
                                        id="contact"
                                        onKeyDown={() =>
                                            openDropDown("contact")
                                        }
                                        tabIndex="0"
                                        className="btn btn--primary btn--sm"
                                    >
                                        Contact
                                    </div>
                                </DropDown>
                            </div>
                        )}

                        <Container>
                            <FlexRow>
                                {d.images.length > 0 ? (
                                    d.images.map(i => (
                                        <FlexItem key={i.uuid}>
                                            <img src={i.url} />
                                        </FlexItem>
                                    ))
                                ) : (
                                    <Note>
                                        No images were uploaded for this unit
                                    </Note>
                                )}
                            </FlexRow>
                            <div className="margin-top--sm" />
                            <Line />

                            <b className="text--md">Figures</b>
                            <div
                                style={{ minHeight: height }}
                                className="margin-bottom--xl margin-top--sm"
                            >
                                <span
                                    style={{
                                        display: "inline-block",
                                        paddingRight: "var(--space-sm)"
                                    }}
                                >
                                    <small className="text--sm text--grey">
                                        monthly
                                    </small>
                                    <div className="text--lg text--bold text--green">
                                        ${d.price_fee}{" "}
                                    </div>{" "}
                                </span>

                                <span style={{ display: "inline-block" }}>
                                    <small className="text--sm text--grey">
                                        security deposit
                                    </small>
                                    <div className="text--lg text--bold text--green">
                                        {d.security_deposit
                                            ? `$${d.security_deposit}`
                                            : "none"}{" "}
                                    </div>{" "}
                                </span>
                            </div>

                            <Line />
                            <b className="text--md">Description</b>
                            <div
                                style={{ minHeight: height }}
                                className="margin-bottom--sm margin-top--md"
                            >
                                <p className="margin-bottom--xl">
                                    {d.unit_description}
                                </p>
                            </div>

                            <Line />

                            <b className="text--md">Location</b>
                            <div
                                style={{ minHeight: height }}
                                className="margin-bottom--xl margin-top--sm"
                            >
                                <p>
                                    {d.parent_property.street},{" "}
                                    {d.parent_property.city},{" "}
                                    {d.parent_property.state} <br />
                                    <ExternalLink
                                        to={`https://www.google.com/maps/search/${d.parent_property.street},${d.parent_property.city},${d.parent_property.state}`}
                                    >
                                        View on google map
                                    </ExternalLink>
                                </p>
                            </div>
                            <Line />
                            <b className="text--md">Listed by</b>
                            <div
                                style={{ minHeight: height }}
                                className="margin-bottom--sm margin-top--md"
                            >
                                <Avatar
                                    first_name={
                                        d.parent_property.owner_first_name
                                    }
                                    last_name={
                                        d.parent_property.owner_last_name
                                    }
                                    size="large"
                                    src={d.parent_property.owner_avatar}
                                />
                                <AvatarLabel yAxis={17}>
                                    <Space amount={2} />
                                    {d.parent_property.owner_first_name}
                                </AvatarLabel>
                            </div>
                        </Container>
                    </section>
                )}
            </Page>
        );
    }
}
export default ListingPage;
