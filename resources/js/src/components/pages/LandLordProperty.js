import Badge from "./../ui/surfaces/Badge";
import Container from "../ui/content/Container";
import Head from "./../elements/Head";
import { OverviewContext } from "../elements/OverviewRoute";
import Page from "../elements/Page";
import { PropertyTable } from "../ui/content/Tables";
import React from "react";

function Property(props) {
    return (
        <OverviewContext.Consumer>
            {context => (
                <Page>
                    <Head title="Properties" />
                    <span
                        aria-label="a table with all properties, can be expanded for more details"
                        data-balloon-pos="right"
                        data-balloon-length="medium"
                    >
                        <Badge>?</Badge>
                    </span>
                    <div className="margin-bottom--xxs" />
                    <Container>
                        <PropertyTable
                            data={context.state.userProperties}
                            loading={props.isLoading}
                            refresh={props.refresh}
                        />
                    </Container>
                </Page>
            )}
        </OverviewContext.Consumer>
    );
}

export default Property;
