import { FlexItem, FlexRow } from "../elements/FlexView";
import { OVERVIEW, PROPERTY, UNIT } from "../../constants/Routes";
import { PropertyForm, UnitForm } from "../ui/inputs/Form";
import React, { Component } from "react";

import Badge from "../ui/surfaces/Badge";
import { BaseCard } from "../ui/surfaces/Card";
import Container from "../ui/content/Container";
import { Line } from "../ui/content/Line";
import { Link } from "react-router-dom";
import Modal from "react-modal";
import { ModalHeader } from "../ui/surfaces/Modal";
import { OverviewContext } from "../elements/OverviewRoute";
import Page from "../elements/Page";

class Overview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            modalTitle: null,
            content: null
        };
        this.setState = this.setState.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    openModal(id) {
        this.renderContent(id);
    }

    closeModal() {
        this.props.refresh();
        this.setState({ modalIsOpen: false });
    }

    renderContent(key) {
        switch (key) {
            case 1:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Add Property",
                    content: (
                        <div className="grid-center">
                            <PropertyForm closeModal={this.closeModal} />
                        </div>
                    )
                });
                break;
            case 2:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Add Unit",
                    content: (
                        <OverviewContext.Consumer>
                            {overview => (
                                <div className="grid-center">
                                    <UnitForm
                                        closeModal={this.closeModal}
                                        data={overview.state.userProperties}
                                    />
                                </div>
                            )}
                        </OverviewContext.Consumer>
                    )
                });
                break;
            case 3:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Limit reached for now",
                    content: (
                        <div className="grid-center">
                            <div style={{ maxWidth: "350px" }}>
                                <h4 className="margin-top--md text--accent-light text--center">
                                    You've reached your limit
                                </h4>
                                <p className="text--center">
                                    Currently you get to list <b>1 property</b>{" "}
                                    and <b>3 units</b>.
                                </p>
                            </div>
                        </div>
                    )
                });
                break;
            case 4:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Getting Started",
                    content: (
                        <div>
                            <iframe
                                width="100%"
                                height="500px"
                                src="https://www.youtube.com/embed/eKDxaqtfvlA"
                            />
                            <div className="grid-center">
                                <p
                                    style={{ maxWidth: "400px" }}
                                    className="text--center margin-top--sm"
                                >
                                    Our quick start guide to get you up and
                                    running with Enproperty.
                                </p>
                            </div>
                        </div>
                    )
                });
                break;
            default:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Modal",
                    content: (
                        <div className="grid-center">
                            <b className="margin-top--lg margin-bottom--lg">
                                Default
                            </b>
                        </div>
                    )
                });
                break;
        }
    }

    render() {
        return (
            <OverviewContext.Consumer>
                {context => (
                    <Page>
                        <span
                            aria-label="overview is where you add units and properties"
                            data-balloon-pos="right"
                            data-balloon-length="medium"
                        >
                            <Badge>?</Badge>
                        </span>
                        <div className="margin-bottom--xxs" />
                        <Container>
                            <ButtonBar
                                isLoading={context.state.isLoading}
                                data={[
                                    context.state.userProperties,
                                    context.state.userUnits
                                ]}
                                openModal={this.openModal}
                            />
                            <div className="margin-bottom--sm" />
                            <Line />
                            <div className="margin-bottom--md" />
                            <Stats
                                data={[
                                    context.state.userProperties,
                                    context.state.userUnits
                                ]}
                            />
                            <Modal
                                isOpen={this.state.modalIsOpen}
                                onRequestClose={this.closeModal}
                                contentLabel="Modal"
                                className="modal-content"
                                overlayClassName="modal-overlay"
                            >
                                <ModalHeader
                                    modalTitle={this.state.modalTitle}
                                    closeModal={this.closeModal}
                                    blur={
                                        context.state.userProperties.length == 1
                                            ? true
                                            : false
                                    }
                                />
                                {this.state.content}
                                <div className="margin-bottom--lg" />
                            </Modal>
                        </Container>
                    </Page>
                )}
            </OverviewContext.Consumer>
        );
    }
}

function Stats(props) {
    return (
        <FlexRow>
            <FlexItem>
                <BaseCard width={200}>
                    <div
                        style={{ fontSize: "5em" }}
                        className="text--accent-light text--bold text--center"
                    >
                        {props.data[0].length}
                    </div>

                    <p>
                        <b className="text--md">Total Properties</b>
                    </p>

                    <div className="btns">
                        <Link
                            to={OVERVIEW + PROPERTY}
                            className="btn btn--secondary btn--full-width "
                            tabIndex="0"
                        >
                            view
                        </Link>
                    </div>
                </BaseCard>
            </FlexItem>
            <FlexItem>
                <BaseCard width={200}>
                    <div
                        style={{ fontSize: "5em" }}
                        className="text--accent-light text--bold text--center"
                    >
                        {props.data[1].length}
                    </div>

                    <p>
                        <b className="text--md">Total Units</b>
                    </p>

                    <div className="btns">
                        <Link
                            to={OVERVIEW + UNIT}
                            className="btn text--sm btn--secondary btn--full-width "
                            tabIndex="0"
                        >
                            view
                        </Link>
                    </div>
                </BaseCard>
            </FlexItem>
        </FlexRow>
    );
}

function ButtonBar(props) {
    return (
        <FlexRow>
            <FlexItem>
                <button
                    onClick={
                        props.data[0].length == 1
                            ? () => props.openModal(3)
                            : () => props.openModal(1)
                    }
                    disabled={props.isLoading}
                    className="btn btn--primary btn--md"
                >
                    Add Property
                </button>
            </FlexItem>
            <FlexItem>
                <button
                    onClick={
                        props.data[1].length >= 3
                            ? () => props.openModal(3)
                            : () => props.openModal(2)
                    }
                    className="btn btn--secondary btn--md"
                    disabled={props.isloading || props.data[0].length == 0}
                >
                    Add Unit
                </button>
            </FlexItem>
            <FlexItem>
                <button
                    onClick={() => props.openModal(4)}
                    className="btn btn--secondary btn--md"
                >
                    Starting Guide
                </button>
            </FlexItem>
        </FlexRow>
    );
}

export default Overview;
