import { ExternalLink } from "../navigation/CustomLink";
import { Link } from "react-router-dom";
import React from "react";
import { TERMS } from "../../../constants/Routes";
import styled from "styled-components";

const StyledFooter = styled.div`
    background: var(--black);

    .footer,
    .fh,
    .fp {
        color: var(--white) !important;
    }

    .footer-section-head {
        border-bottom: 2px solid var(--white);
        padding-bottom: var(--space-xs);
        font-weight: bold;
    }
    .footer-list {
        list-style-type: none;
        margin: 1.5em 3.2em;
        margin-left: -10px;
    }

    .footer-list li {
        padding: 6px 4px;
    }

    .footer-list li a {
        color: var(--white);
        text-decoration: none;
        padding: 2px 2px;
    }

    .footer-list li a:hover {
        color: var(--accent-color);
    }

    .footer-section {
        display: flex;
        flex-wrap: wrap;
        -webkit-align-items: baseline;
        -ms-flex-align: baseline;
        align-items: baseline;
        padding-bottom: var(--space-xxl);
    }
`;

let date = new Date();
const Footer = () => (
    <StyledFooter className="grid-center">
        <div className="footer-section ">
            <ul className="footer-list">
                <li>
                    <h5 className="footer-section-head fh">Company</h5>
                </li>
                {/* <li>
                    <Link tabIndex="0" to="/">
                        About
                    </Link>
                </li> */}
                <li>
                    <ExternalLink
                        to="mailto:enproperty.co@gmail.com"
                        color="var(--white)"
                    >
                        Contact us
                    </ExternalLink>
                </li>
                <li>
                    <Link tabIndex="0" to={TERMS}>
                        Terms of service
                    </Link>
                </li>
            </ul>

            <ul className="footer-list">
                <li>
                    <h5 className="footer-section-head fh">Product</h5>
                </li>
                {/* <li>
                    <Link tabIndex="0" to="/">
                        Why Enproperty
                    </Link>
                </li> */}
                {/* <li>
                    <Link tabIndex="0" to="/">
                        Pricing
                    </Link>
                </li> */}
                {/* <li>
                    <Link tabIndex="0" to="/">
                        Roadmap
                    </Link>
                </li> */}
                <li>
                    <ExternalLink
                        to="https://enproperty-co.typeform.com/to/Eyhj8j"
                        color="var(--white)"
                    >
                        Feedback
                    </ExternalLink>
                </li>
                <li>
                    <ExternalLink
                        to="https://enproperty.crisp.watch/en/"
                        color="var(--white)"
                    >
                        Status
                    </ExternalLink>
                </li>
            </ul>

            {/* <ul className="footer-list">
                <li>
                    <h5 className="footer-section-head fh">Resources</h5>
                </li>
                <li>
                    <Link tabIndex="0" to="/">
                        Help Center
                    </Link>
                </li>
                <li>
                    <Link tabIndex="0" to="/">
                        Privacy & Security
                    </Link>
                </li>
                <li>
                    <Link tabIndex="0" to="/">
                        Blog
                    </Link>
                </li>
                <li>
                    <Link tabIndex="0" to="/">
                        Press Kit
                    </Link>
                </li>
            </ul> */}
        </div>
        <div className="grid-center footer-legal">
            <p className="fp">Made with ❤ in Kingston, Jamaica</p>
        </div>
        <div className="grid-center footer-legal margin-bottom--lg">
            <p className="fp">
                © Copyright 2018 - {date.getFullYear()} Enproperty
            </p>
        </div>
    </StyledFooter>
);
export default Footer;
