import {
    CurrencyInput,
    FileUpload,
    GenericField,
    Label,
    PasswordField,
    PhoneInput,
    ProfileImageField,
    ToggleInput,
    handleBlur,
    handleChange,
    unMaskCurrency,
    unMaskPhone
} from "./Field";
import { Form, Formik } from "formik";
import {
    LOGIN,
    PASSWORD_RESET,
    SIGN_UP,
    TERMS
} from "../../../constants/Routes";
import {
    LoginSchema,
    LogTimeSchema,
    PasswordResetSchema,
    PropertySchema,
    SignupSchema,
    UnitSchema,
    UpdatePasswordSchema,
    UserCellSchema,
    UserEmailSchema,
    UserNameSchema,
    VerificationCodeSchema
} from "../../../helper/validation";
import {
    createTimesheet,
    createProperty,
    createUnit,
    forgotPassword,
    login,
    updateProperty,
    updateUnit,
    updateUser
} from "../../../helper/apiHelper";
import { del, get } from "idb-keyval";

import CustomLink from "../navigation/CustomLink";
import { FORMS_COPY } from "../../../constants/copy";
import Note from "../content/Note";
import React from "react";
import { StyledToggleInputWrapper } from "./Field";
import { authenticateUser } from "../../../helper/authHelper";
import { device } from "../../../constants/devices";
import notification from "../../../helper/notificationHelper";
import styled from "styled-components";

export const OverideForm = styled.div`
    & form {
        border: 1px solid transparent;
        padding: 0px;
    }
    & form > button {
        margin-top: 24px;
        margin-bottom: 24px;
    }
`;

const StyledForm = styled(Form)`
    background-color: var(--primary-color-2);
    border: 1px solid #e0e6e8;
    border-radius: var(--radius);
    padding: 20px;
    margin-top: 30px;
    max-width: 500px;
    min-width: 400px;
    > input,
    > select,
    > div > input,
    > div > div > input,
    > textarea,
    > div > textarea,
    > div > div > textarea {
        width: 100%;
        display: block;
        font-size: var(--text-sm);
        padding: 12px 20px;
        margin: 4px 0px 18px 0px;
        box-sizing: border-box;
        border: 2px solid #ccc;
        border-radius: 6px;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        outline: none;
    }
    textarea {
        max-width: 100%;
        min-height: 200px;
    }
    > input:focus,
    > div > input:focus,
    > div > div > input:focus,
    > textarea:focus,
    > div > textarea:focus,
    > div > div > textarea:focus {
        border: 2px solid var(--secondary-color);
    }

    > ::placeholder {
        color: var(--light-grey);
    }

    @media ${device.tablet} {
        min-width: 300px;
    }

    @media ${device.mobileL} {
        min-width: 240px;
        max-width: 300px;
    }
`;

const StyledFormFlex = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between !important;
`;

const StyledFormInline = styled.div`
    display: inline-block !important;
    padding: auto;import notification from './../../helper/notificationHelper';
import { UserUpdateSchema } from './../../helper/validation';
import { yup } from 'yup';
import Thumb from './Thumb';
import { values } from '../../../../../public/js/app';
import { UserCellSchema } from './../../../helper/validation';
import { Note } from '../content/Note';

`;


export const LoginForm = () => (
    <Formik
        validationSchema={LoginSchema}
        onSubmit={(values, { setSubmitting }) => {
            delete values.show;
            login(values)
                .then(res => {
                    authenticateUser(res.data.access_token);
                    location.reload();
                })
                .catch(function(error) {
                    if (error.response) {
                        notification.danger(error.response.data.message);
                    }
                });
            setTimeout(() => {
                setSubmitting(false);
            }, 7000);
        }}
    >
        {({ errors, touched, values, isSubmitting }) => (
            <StyledForm autoComplete="on">
                <h3>Login</h3>
                <GenericField
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="eg. jean@gmail.com"
                    errors={errors.email}
                    touched={touched.email}
                />
                <PasswordField
                    submit={isSubmitting}
                    values={values}
                    errors={errors}
                    touched={touched}
                />
                <button
                    disabled={isSubmitting}
                    className="btn btn--primary btn--full-width margin-top--md"
                    type="submit"
                >
                    {isSubmitting ? "Loading" : "Login"}
                </button>
            </StyledForm>
        )}
    </Formik>
);

export const LogTimeForm = props => (
    <Formik
        initialValues={{
            task: "",
            client_id:props.data[0].id,
            date: "",
            from: "2019-11-01T20:00",
            to: "2019-11-01T17:00",
            comment: ""
        }}
        validationSchema={LogTimeSchema}
        onSubmit={(values, { setSubmitting }) => {
            values.client_id = parseInt(values.client_id)
            console.log(values);
            // createTimesheet(values).then(res=>{
            //     notification.success(res.message);
                    // props.closeModal
                    // ? props.closeModal()
                    // : null;
            // })
            setTimeout(() => {
                setSubmitting(false);
                props.closeModal
            }, 2000);
        }}
    >
        {({
            errors,
            touched,
            isSubmitting,
            isValid,
            values,
            handleBlur,
            handleChange
        }) => (
            <StyledForm>
                <Label className="margin-bottom--lg">Select client</Label>
                <select
                    name="client_id"
                    className=" margin-bottom--sm margin-top--sm"
                    value={values.client_id}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    style={{
                        fontWeight: "normal",
                        fontSize: "var(--text-sm)"
                    }}
                >
                    {props.data.map(d => (
                        <option key={d.id} value={d.id}>
                            {d.name}
                        </option>
                    ))}
                </select>

                <GenericField
                    label="Task"
                    name="task"
                    type="text"
                    placeholder="task or assignment"
                    errors={errors.task}
                    touched={touched.task}
                />

                <GenericField
                    label="Comment"
                    name="comment"
                    component="textarea"
                    placeholder="comments"
                    errors={errors.task}
                    touched={touched.task}
                />
                <GenericField
                    label="Date"
                    name="date"
                    type="date"
                    placeholder=""
                    errors={errors.date}
                    touched={touched.date}
                />
                <GenericField
                    label="From"
                    name="from"
                    type="datetime-local"
                    placeholder=""
                    errors={errors.from}
                    touched={touched.from}
                />
                <GenericField
                    label="To"
                    name="to"
                    type="datetime-local"
                    placeholder="e"
                    errors={errors.to}
                    touched={touched.to}
                />
                <button
                    disabled={isSubmitting || !isValid}
                    className="btn btn--primary btn--full-width margin-top--md"
                    type="submit"
                >
                    {isSubmitting ? "Loading" : "Submit for approval"}
                </button>
            </StyledForm>
        )}
    </Formik>
);

export const UpdatePasswordForm = props => (
    <Formik
        validationSchema={UpdatePasswordSchema}
        initialValues={{
            password: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
            get("reset")
                .then(res => {
                    values.email = res.email;
                    values.token = props.match.params.token;
                    values.password_confirmation = values.password;

                    updatePassword(values)
                        .then(res => {
                            del("reset");
                            notification.success("Password updated");
                            setTimeout(() => {
                                // setSubmitting(false);
                            }, 2000);
                        })
                        .catch(error => {
                            if (error.response) {
                                notification.danger(
                                    error.response.data.message
                                );
                                del("reset");
                            }
                        });
                })
                .catch(() => {
                    notification.danger("This reset token is invalid");
                });
            setTimeout(() => {
                setSubmitting(false);
            }, 2000);
        }}
    >
        {({ errors, values, touched, isSubmitting, isValid }) => (
            <StyledForm>
                <h3>New Password</h3>
                <PasswordField
                    submit={isSubmitting}
                    values={values}
                    errors={errors}
                    touched={touched}
                />
                <button
                    disabled={isSubmitting || !isValid}
                    className="btn btn--primary btn--full-width margin-top--md"
                    type="submit"
                >
                    {isSubmitting ? "Loading" : "Update Password"}
                </button>
                <p className="text--sm">
                    <CustomLink to={LOGIN}>Back to login</CustomLink>
                </p>
            </StyledForm>
        )}
    </Formik>
);

export function UpdateNameForm(props) {
    return (
        <Formik
            validationSchema={UserNameSchema}
            initialValues={{ firstName: "", lastName: "" }}
            onSubmit={(values, { setSubmitting }) => {
                values.first_name = values.firstName;
                values.last_name = values.lastName;
                values.cell = props.data.cell;
                delete values.firstName;
                delete values.lastName;
                setTimeout(() => {
                    updateUser(values)
                        .then(res => {
                            notification.success(res.data.message, 8000);
                            del("user").then(() => {
                                location.reload();
                            });
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response);
                            }
                        });
                    setTimeout(() => {
                        setSubmitting(false);
                    }, 2000);
                }, 3000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm>
                    <StyledFormFlex>
                        <StyledFormInline>
                            <GenericField
                                label="First Name"
                                name="firstName"
                                errors={errors.firstName}
                                touched={touched.firstName}
                            />
                        </StyledFormInline>
                        <StyledFormInline>
                            <GenericField
                                label="Last Name"
                                name="lastName"
                                errors={errors.lastName}
                                touched={touched.lastName}
                            />
                        </StyledFormInline>
                    </StyledFormFlex>

                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--sm "
                        type="submit"
                    >
                        {isSubmitting ? "Saving" : "Save"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function UpdateEmailForm(props) {
    return (
        <Formik
            validationSchema={UserEmailSchema}
            initialValues={{ email: props.email }}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    updateUser(values)
                        .then(res => {
                            notification.success(res.data.message, 5000);
                            del("user").then(() => {
                                location.reload();
                            });
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response);
                            }
                        });
                    setTimeout(() => {
                        setSubmitting(false);
                    }, 2000);
                }, 3000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm>
                    <GenericField
                        label="Email"
                        name="email"
                        type="email"
                        placeholder="eg. jean@gmail.com"
                        errors={errors.email}
                        touched={touched.email}
                    />
                    <button
                        disabled={isSubmitting || isValid == false}
                        className="btn btn--primary btn--sm "
                        type="submit"
                    >
                        {isSubmitting ? "Saving" : "Save"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function PropertyForm(props) {
    return (
        <Formik
            validationSchema={props.update ? null : PropertySchema}
            initialValues={
                props.update
                    ? {
                          property_title: props.update.property_title,
                          property_id: props.update.uuid,
                          property_description:
                              props.update.property_description,
                          street: props.update.street,
                          city: props.update.city,
                          state: props.update.state
                      }
                    : {
                          property_title: "",
                          property_description: "",
                          street: "",
                          city: "",
                          state: ""
                      }
            }
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    function Create(values) {
                        createProperty(values)
                            .then(res => {
                                notification.success("Property added", 5000);
                                {
                                    props.closeModal
                                        ? props.closeModal()
                                        : null;
                                }
                            })
                            .catch(error => {
                                if (error.response) {
                                    notification.warning(
                                        error.response.data.message,
                                        5000
                                    );
                                    props.closeModal
                                        ? props.closeModal()
                                        : null;
                                }
                            });
                    }

                    function Update(values) {
                        updateProperty(values).then(() => {
                            notification.success("Property updated", 5000);
                            {
                                props.closeModal ? props.closeModal() : null;
                            }
                        });
                    }

                    props.update ? Update(values) : Create(values);

                    setTimeout(() => {
                        setSubmitting(false);
                    }, 2000);
                }, 3000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm id="s4">
                    <GenericField
                        label="Title"
                        name="property_title"
                        placeholder="eg. Double Bedroom shared facilities"
                        errors={errors.property_title}
                        touched={touched.property_title}
                    />
                    <GenericField
                        label="Description"
                        placeholder="Start Writing here"
                        name="property_description"
                        errors={errors.property_description}
                        touched={touched.property_description}
                        component="textarea"
                    />
                    <GenericField
                        label="Street"
                        name="street"
                        errors={errors.street}
                        touched={touched.street}
                    />
                    <GenericField
                        label="Town / City"
                        name="city"
                        errors={errors.city}
                        touched={touched.city}
                    />
                    <GenericField
                        label="Parish"
                        name="state"
                        errors={errors.state}
                        touched={touched.state}
                    />
                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--full-width "
                        type="submit"
                    >
                        {isSubmitting
                            ? "Loading"
                            : props.update
                            ? "Save changes"
                            : "Add Property"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function UnitForm(props) {
    return (
        <Formik
            validationSchema={props.update ? null : UnitSchema}
            initialValues={
                props.update
                    ? {
                          unit_title: props.update.unit_title,
                          unit_description: props.update.unit_description,
                          price_fee: props.update.price_fee,
                          security_deposit: props.update.security_deposit,
                          listed: props.update.listed,
                          property_id: props.update.parent_property.id,
                          unit_id: props.update.uuid
                      }
                    : {
                          unit_title: "",
                          unit_description: "",
                          price_fee: "",
                          security_deposit: "0",
                          listed: true,
                          property_id: props.data.id
                      }
            }
            onSubmit={(values, { setSubmitting }) => {
                if (
                    values.security_deposit != null ||
                    values.security_deposit != undefined
                ) {
                    values.security_deposit = unMaskCurrency(
                        values.security_deposit
                    );
                }

                values.price_fee = unMaskCurrency(values.price_fee);
                function Update(data) {
                    updateUnit(data).then(res => {
                        {
                            data.file
                                ? File(data.file, res.data)
                                : notification.success("Unit updated", 5000);
                        }
                        {
                            props.closeModal ? props.closeModal() : null;
                        }
                    });
                }
                function Create(data) {
                    createUnit(data)
                        .then(res => {
                            {
                                data.file
                                    ? File(data.file, res.data)
                                    : notification.success("Unit added", 5000);
                            }
                            {
                                props.closeModal ? props.closeModal() : null;
                            }
                        })
                        .catch(error => {
                            if (error.response) {
                                notification.warning(
                                    error.response.data.message,
                                    12000
                                );
                                {
                                    props.closeModal
                                        ? props.closeModal()
                                        : null;
                                }
                            }
                        });
                }
                function File(file, id) {
                    let formData = new FormData();
                    for (let i = 0; i < file.length; i++) {
                        formData.append(`file[${[i]}]`, file[i]);
                    }
                    formData.append("id", id);
                    uploadUnitImage(formData).then(res => {
                        notification.success("Success", 8000);
                        setSubmitting(false);
                    });
                }
                function Errors(message) {
                    notification.danger(message, 8000);
                    setSubmitting(false);
                }

                //if files are selected start validating
                if (values.file) {
                    const config = {
                        maxSize: 2,
                        fileLimit: 6,
                        compress: true,
                        extensions: ["image/png", "image/jpeg"]
                    };
                    FileValidate(values.file, config).then(res => {
                        values.file = res.file;
                        res.error
                            ? Errors(res.message)
                            : props.update
                            ? Update(values)
                            : Create(values);
                    });
                }
                // if no files then do a regular create or update
                else {
                    {
                        props.update ? Update(values) : Create(values);
                        setTimeout(() => {
                            setSubmitting(false);
                        }, 1000);
                    }
                }
            }}
        >
            {({
                errors,
                touched,
                isSubmitting,
                isValid,
                values,
                setFieldValue,
                handleBlur,
                handleChange
            }) => (
                <StyledForm>
                    {props.update ? null : (
                        <React.Fragment>
                            <Label className="margin-bottom--lg">
                                Property to add unit
                            </Label>
                            <select
                                name="property_id"
                                className=" margin-bottom--sm margin-top--sm"
                                value={values.property_id}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                style={{
                                    fontWeight: "normal",
                                    fontSize: "var(--text-sm)"
                                }}
                            >
                                <option />
                                {props.data.map(d => (
                                    <option key={d.uuid} value={d.uuid}>
                                        {d.property_title}
                                    </option>
                                ))}
                            </select>
                        </React.Fragment>
                    )}
                    <GenericField
                        label="Title"
                        name="unit_title"
                        placeholder="eg. Double Bedroom shared facilities"
                        errors={errors.unit_title}
                        touched={touched.unit_title}
                    />
                    <GenericField
                        label="Description [optional]"
                        placeholder="Start Writing here"
                        name="unit_description"
                        errors={errors.unit_description}
                        touched={touched.unit_description}
                        component="textarea"
                    />
                    <GenericField
                        label="Price"
                        name="price_fee"
                        component={CurrencyInput}
                        errors={errors.price_fee}
                        touched={touched.price_fee}
                    />
                    <GenericField
                        label="Deposit [optional]"
                        name="security_deposit"
                        component={CurrencyInput}
                        errors={errors.security_deposit}
                        touched={touched.security_deposit}
                    />
                    <Label>List this unit</Label>
                    <StyledToggleInputWrapper className="margin-top--xs">
                        <ToggleInput
                            id="true"
                            value={values.listed}
                            name="listed"
                            type="radio"
                            checked={values.listed === true}
                            label="Yes"
                            onChange={() => {
                                setFieldValue("listed", true);
                            }}
                        />
                        <ToggleInput
                            id="false"
                            className="no"
                            type="radio"
                            value={values.listed}
                            name="listed"
                            checked={values.listed === false}
                            label="No"
                            onChange={() => {
                                setFieldValue("listed", false);
                            }}
                        />
                    </StyledToggleInputWrapper>

                    <Label className="margin-top--xs">
                        {props.update ? "Add more photos" : "Photos"}{" "}
                    </Label>

                    <FileUpload value={values} set={setFieldValue} />

                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--full-width "
                        type="submit"
                    >
                        {isSubmitting
                            ? "Loading"
                            : props.update
                            ? "Save changes"
                            : "Add Unit"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}
