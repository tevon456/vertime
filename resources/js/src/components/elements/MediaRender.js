import React from "react";

/**
 * Conditional to render a child component if screen width is greater than min prop.
 * @prop {number} min: Min display width in pixels to render child
 */
function MediaRender(props) {
    let renderValue;
    if (props.min === undefined) {
        renderValue = 0;
    } else {
        renderValue = props.min;
    }
    return (
        <React.Fragment>
            {window.screen.width > renderValue ? (
                <React.Fragment>{props.children}</React.Fragment>
            ) : null}
        </React.Fragment>
    );
}
export default MediaRender;
